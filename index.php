<?php
$stage_file = yaml_parse_file("stages.yml", $pos = 0, $callbacks = null);
$team_file = yaml_parse_file("team.yml", $pos = 0, $callbacks = null);

$output = array();

$groups = array(
	"access" => "eng-dev-manage-access",
	"compliance" => "eng-dev-manage-compliance",
	"import" => "eng-dev-manage-import",
	"optimize" => "eng-dev-manage-optimize",
	"project_management" => "eng-dev-plan-project-mgmt",
	"product_planning" => "eng-dev-plan-product-planning",
	"certify" => "eng-dev-plan-certify",
	"source_code" => "eng-dev-create-source-code",
	"code_review" => "eng-dev-create-code-review",
	"editor" => "eng-dev-create-editor",
	"gitaly" => "eng-dev-create-gitaly",
	"pipeline_execution" => "eng-dev-verify-pipeline-execution",
	"pipeline_authoring" => "eng-dev-verify-pipeline-authoring",
	"runner" => "eng-dev-verify-runner",
	"testing" => "eng-dev-verify-testing",
	"package" => "eng-dev-package-package",
	"static_analysis" => "eng-dev-secure-static-analysis",
	"dynamic_analysis" => "eng-dev-secure-dynamic-analysis",
	"composition_analysis" => "eng-dev-secure-composition-analysis",
	"threat_insights" => "eng-dev-secure-threat-insights",
	"vulnerability_research" => "eng-dev-secure-research",
	"container_security" => "eng-dev-protect-container-security",
	"monitor" => "eng-dev-monitor-monitor",
	"release" => "eng-dev-release-release-mgmt",
	"configure" => "eng-dev-configure-configure",
	"purchase" => "eng-dev-fulfillment-purchase",
	"license" => "eng-dev-fulfillment-license",
	"utilization" => "eng-dev-fulfillment-utilization",
	"activation" => "eng-dev-growth-activation",
	"conversion" => "eng-dev-growth-conversion",
	"expansion" => "eng-dev-growth-expansion",
	"adoption" => "eng-dev-growth-adoption",
	"product_intelligence" => "eng-dev-growth-product-intelligence",
	"distribution" => "eng-dev-enablement-distribution",
	"geo" => "eng-dev-enablement-geo",
	"memory" => "eng-dev-enablement-memory",
	"global_search" => "eng-dev-enablement-search",
	"database" => "eng-dev-enablement-database",
	"sharding" => "eng-dev-enablement-sharding",
	"applied_ml" => "eng-dev-modelops-applied-ml",
	"mlops" => "eng-dev-modelops-ml-ops",
	"dataops" => "eng-dev-modelops-data-ops",
	"moble_devops" => "eng-dev-mobile-devops-apps",
	"5-min-app" => "eng-dev-deploy-five-min-app"
);

$unsetKey = array(
	"name",
	"focus",
	"be_team_tag",
	"fe_team_tag",
	"cs_team_tag",
	"internal_customers",
	"categories",
	"usage_driver_score",
	"asp_driver_score",
	"sam_driver_score",
	"group_link",
	"pi_gmau",
	"pi_pgmau",
	"analyst_reports",
	"comp_comparison",
	"handbook",
	"direction",
	"channel",
	"alias",
	"gitlab_group",
	"google_group",
	"slack",
	"description",
	"image",
	"roadmap",
	"direction"
);

$stages = $stage_file['stages'];

$team = $team_file;

$i = 0;

foreach ($stages as $stage => $stagesArr)
{

	foreach ($stages[$stage]['groups'] as $gkey => $gvalue)
	{
		$j = 0;
		$k = 0;
		if (array_key_exists($gkey, $groups))
		{
			$output[$i]['group_name'] = $groups[$gkey];
			foreach ($gvalue as $grkey => $grvalue)
			{
				if ($grkey == "backend_engineering_manager" || $grkey == "frontend_engineering_manager")
				{
					$output[$i]['group_managers'][$k]['member_name'] = $grvalue;
					foreach ($team as $tkey => $tvalue)
					{
						if ($tvalue['name'] == $grvalue)
						{
							$output[$i]['group_managers'][$k]['member_handle'] = $tvalue['slug'];
						}
					}
					$k++;
				}
				elseif (!in_array($grkey, $unsetKey))
				{
					if (is_array($grvalue))
					{
						foreach ($grvalue as $grrkey => $grrvalue)
						{

							$output[$i]['group_members'][$j]['member_name'] = $grrvalue;
							foreach ($team as $tkey => $tvalue)
							{
								if ($tvalue['name'] == $grrvalue)
								{

									$output[$i]['group_members'][$j]['member_handle'] = $tvalue['slug'];
								}
							}
							$j++;
						}

					}
					else
					{

						$output[$i]['group_members'][$j]['member_name'] = $grvalue;
						foreach ($team as $tkey => $tvalue)
						{
							if ($tvalue['name'] == $grvalue)
							{

								$output[$i]['group_members'][$j]['member_handle'] = $tvalue['slug'];
							}
						}
						$j++;
					}

				}

			}
		}
		$i++;
	}
}

echo "<pre>";
print_r($output);
echo "</pre>";
?>
